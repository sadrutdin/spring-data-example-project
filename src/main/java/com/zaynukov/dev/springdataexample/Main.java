package com.zaynukov.dev.springdataexample;

import com.zaynukov.dev.springdataexample.domain.CityEntity;
import com.zaynukov.dev.springdataexample.domain.CountryEntity;
import com.zaynukov.dev.springdataexample.repository.CountryCustomRepository;
import com.zaynukov.dev.springdataexample.repository.CountryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.Optional;
import java.util.Set;

@SpringBootApplication
public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    private final CountryRepository countryRepository;

    public Main(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(Main.class, args);

        CountryCustomRepository repository = context.getBean(CountryCustomRepository.class);
        Optional<CountryEntity> optional = repository.findById(1);
        logger.info(optional.toString());
        
        Main main = (Main) context.getBean("main");
        main.insertTestValues();
        main.countryRepository.findAll().forEach(System.out::println);
    }

    private void insertTestValues() {
        if (countryRepository.count() == 0) {
            CountryEntity country1 = new CountryEntity().setName("Россия").setCode("РОС");
            Set<CityEntity> citySet1 = country1.getCitySet();
            citySet1.add(new CityEntity().setCountry(country1).setCode("МХКЛ").setName("Махачкала"));
            citySet1.add(new CityEntity().setCountry(country1).setCode("МСК").setName("Москва"));
            citySet1.add(new CityEntity().setCountry(country1).setCode("СПБ").setName("Санкт-Петербург"));
            countryRepository.save(country1);

            CountryEntity country2 = new CountryEntity().setName("United States of America").setCode("USA");
            Set<CityEntity> citySet2 = country2.getCitySet();
            citySet2.add(new CityEntity().setCountry(country2).setCode("NYC").setName("New-York"));
            citySet2.add(new CityEntity().setCountry(country2).setCode("WSHT").setName("Washington"));
            citySet2.add(new CityEntity().setCountry(country2).setCode("LAC").setName("Los-Angeles"));
            countryRepository.save(country2);

            CountryEntity country3 = new CountryEntity().setName("Deutschland").setCode("DSCHL");
            Set<CityEntity> citySet3 = country3.getCitySet();
            citySet3.add(new CityEntity().setCountry(country3).setCode("BRLN").setName("Berlin"));
            citySet3.add(new CityEntity().setCountry(country3).setCode("STGRT").setName("Stuttgart"));
            citySet3.add(new CityEntity().setCountry(country3).setCode("MNCHN").setName("München"));
            countryRepository.save(country3);
        }
    }

}
