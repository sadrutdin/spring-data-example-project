package com.zaynukov.dev.springdataexample.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@EqualsAndHashCode(exclude = "citySet")
@ToString(exclude = "citySet")
@Accessors(chain = true)
@Entity
@Table(name = "countries")
public class CountryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(length = 20, nullable = false, unique = true)
    private String code;
    @OneToMany(mappedBy = "country", cascade = CascadeType.ALL)
    private Set<CityEntity> citySet = new HashSet<>();
}
