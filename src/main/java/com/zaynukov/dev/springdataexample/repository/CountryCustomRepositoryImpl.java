package com.zaynukov.dev.springdataexample.repository;

import com.zaynukov.dev.springdataexample.domain.CountryEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Optional;

@Repository
public class CountryCustomRepositoryImpl implements CountryCustomRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public Optional<CountryEntity> findById(long id) {
        return Optional.ofNullable(em.find(CountryEntity.class, id));
    }

    @Override
    public CountryEntity persist(CountryEntity e) {
        em.persist(e);
        return e;
    }

    @Override
    public CountryEntity merge(CountryEntity e) {
        return em.merge(e);
    }

    @Override
    public CountryEntity delete(CountryEntity e) {
        em.remove(e);
        return e;
    }

    @Override
    public boolean deleteById(long id) {
        Optional<CountryEntity> optional = findById(id);
        if (optional.isPresent()) {
            em.remove(optional.get());
            return true;
        } else {
            return false;
        }
    }
}
