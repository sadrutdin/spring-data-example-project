package com.zaynukov.dev.springdataexample.repository;

import com.zaynukov.dev.springdataexample.domain.CountryEntity;

import java.util.Optional;

public interface CountryCustomRepository {
    Optional<CountryEntity> findById(long id);

    CountryEntity persist(CountryEntity e);

    CountryEntity merge(CountryEntity e );

    CountryEntity delete(CountryEntity e);

    boolean deleteById(long id);
}
